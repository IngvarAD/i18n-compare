import unittest
from i18nCompare import I18nCompare

class TestCompare(unittest.TestCase):
    TEST_FROM_YAML = """
        en:
            hello_world: Hello World!
            address: Address
            name: Name
            surname: Surname
    """

    TEST_COMPARE_YAML = """
        de:
            hello_world: Hallo Welt!
            address: Adresse
    """

    def test_all(self):
        comparator = I18nCompare(self.TEST_FROM_YAML, self.TEST_COMPARE_YAML)

        comparator.readSpreadsheet()
        
