## I18nCompare

I created this project to ease process of translating apps made using Ruby on Rails framework. It can create spreadsheet with missing translations, and after filling gaps add it back to YAML dictionary. You are welcome to suggest improvements, changes, make merge requests.

## Dependencies

['requirements.txt'](requirements.txt)
```
PyYAML==5.1.2
pyexcel==0.5.3
argparse==1.1
```

## TODO

1. Check and improve efficiency
2. Fix and improve tests
3. Add gui
4. Make sure it doesn't mess up .yml files
5. Improve Readme.md