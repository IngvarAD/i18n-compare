#!/usr/bin/python3

# This program is for making translations easier
# Made by Igor 'Ingvar' Zapolski

import yaml
import os
import sys
import argparse
from collections import OrderedDict
from pyexcel_ods3 import save_data, get_data

parser = argparse.ArgumentParser(description='Compare translation files and output spreadsheet')
action = parser.add_mutually_exclusive_group()
parser.add_argument('-v', '--verbose', help='Print more info', action='store_true')
action.add_argument('-c', '--compare', help='Compare two YAML files',\
                    action='store_true')
action.add_argument('-e', '--extend', help='Extend YAML file with translation from spreadsheet',\
                    action='store_true')
parser.add_argument('file', type=str, nargs='+')

def verbose(message):
    if args.verbose:
        print(message)

class I18nCompare():
    def __init__(self, fromFile, toFile):
        self.compare_from = yaml.load(open(fromFile, 'r'), Loader=yaml.BaseLoader)
        self.compare_to = yaml.load(open(toFile, 'r'), Loader=yaml.BaseLoader)
        self.from_language = next(iter(self.compare_from))
        self.to_language = next(iter(self.compare_to))
        self.last_breadcrumbs = ()
        self.data = OrderedDict()
        self.sheet_name = 'Default'

    def initializeODS(self):
        header = []
        header.append(["Translating:",self.from_language,self.to_language])
        #header.append(["Note:","%{var} <- this are viariables, do not translate, or modify"])
        header.append(["Key","Value","To translate"])
        self.sheet_name = f'{self.from_language}->{self.to_language}'
        self.data.update({self.sheet_name: header})

    def getDifferencies(self, main_dict, compare_dict, breadcrumbs=[]):
        for key, val in main_dict.items():
            if isinstance(val, dict):
                breadcrumbs.append(key)
                if key in compare_dict:
                    self.getDifferencies(val, compare_dict[key], breadcrumbs)
                else:
                    self.iterate(val, breadcrumbs)
                breadcrumbs.pop(-1)
            elif key in compare_dict:
                if isinstance(compare_dict[key], dict):
                    print("Fucked up")
            else:
                self.addToSpreadsheet(key, val, breadcrumbs)
                verbose(f"Added to ODS: {key}: {val}")

    def addToSpreadsheet(self, key, toTranslate, breadcrumbs):
        if tuple(breadcrumbs) != self.last_breadcrumbs:
            del self.last_breadcrumbs
            self.last_breadcrumbs = tuple(breadcrumbs)
            self.data[self.sheet_name].append(['->' + ';'.join(breadcrumbs)])
        self.data[self.sheet_name].append([key, toTranslate])

    def iterate(self, mainDict, breadcrumbs):
        for key, val in mainDict.items():
            if isinstance(val, dict):
                breadcrumbs.append(key)
                self.iterate(val, breadcrumbs)
                breadcrumbs.pop(-1)
            else:
                self.addToSpreadsheet(key, val, breadcrumbs)

    def getUniqueFilename(self):
        filename = f'{self.from_language}_{self.to_language}'
        i = 0
        while True:
            if not os.path.isfile(filename + '.ods'): break
            i += 1
            filename = filename.replace(str(i-1),'') + str(i)
        return f'{filename}.ods'

    def compareYAMLFiles(self):
        compare = [
            self.compare_from[self.from_language],
            self.compare_to[self.to_language]
            ]
        self.getDifferencies(*compare)
        
        filename = self.getUniqueFilename()
        save_data(filename, self.data)
        verbose(f'Finished. Results saved to {filename}')

    def run(self):
        verbose('Initializing spreadsheet')
        self.initializeODS()
        verbose('Beggining of comparision')
        self.compareYAMLFiles()

class I18nExtend():
    def __init__(self, spreadsheet, translation):
        self.spreadsheet = spreadsheet
        self.breadcrumbs = []
        self.filename = translation.split('/')[-1]
        try:
            with open(translation, 'r') as stream:
                self.translation = yaml.load(stream, Loader=yaml.BaseLoader)
        except FileNotFoundError as not_found:
            print(f'File not found: {not_found}\nTerminating!')
            sys.exit(1)
        except yaml.YAMLError as yml_err:
            print(f'Error occured while parsing yml file: {yml_err}\nTerminating!')
            sys.exit(1)
    
    def run(self):
        self.readSpreadsheet()

    def readSpreadsheet(self):
        translated = get_data(self.spreadsheet)
        translated = translated.pop(list(translated.keys())[0])
        for i, row in enumerate(translated):
            if len(row) in (0,2) or i in (0,1):
                continue
            elif len(row) == 1:
                self.breadcrumbs = row[0].replace('->','').split(';')
                verbose(f'Breadcrumbs: {self.breadcrumbs}')
            else:
                if len(row[0]) == 0:
                    continue
                self.insertToDictionary(row[0], row[2])
                verbose(f'{row[0]} -> {row[2]}')
        self.saveTranslation()
    
    def insertToDictionary(self, key, val):
        dictionary = self.translation
        insert_to = dictionary[next(iter(dictionary))]

        for crumb in self.breadcrumbs:
            insert_to = insert_to[crumb]
        insert_to[key] = val

    def saveTranslation(self):
        filename = self.filename.split('.')[0] + '_extended.yml'
        with open(filename, 'w') as stream:
            yaml.dump(self.translation, stream, encoding='utf-8',allow_unicode=True)

args = parser.parse_args()
if args.compare:
    function = I18nCompare(*args.file)
elif args.extend:
    function = I18nExtend(*args.file)
function.run()
